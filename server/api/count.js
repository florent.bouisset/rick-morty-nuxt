let count = 0;

async function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export default defineEventHandler(async (event) => {
  count++;
  await sleep(3000);
  return count;
});
